#!/usr/bin/env python
# coding: utf-8

# In[1]:


import mwclient
from datetime import datetime
import locale
import requests
from settings import USERNAME, PASSWORD, ADA_S3_BUCKET, ADA_S3_REGION

# Page to read
PAGE_TITLE_IN = 'Opinionsundersökningar inför riksdagsvalet i Sverige 2026'

# Page to write to
PAGE_TITLE_OUT = PAGE_TITLE_IN #"Användare:Jensilainen/sandlåda"

# Where to fetch Ada data
DATA_URL = f"https://{ADA_S3_BUCKET}.s3.{ADA_S3_REGION}.amazonaws.com/"

COMMIT_MSG = "Ny sammanvägning från Botten Ada (automatisk uppdatering)."

# Set the locale to Swedish
locale.setlocale(locale.LC_TIME, 'sv_SE.UTF-8')

site = mwclient.Site('sv.wikipedia.org')
site.login(USERNAME, PASSWORD)


# In[2]:


page_in = site.pages[PAGE_TITLE_IN]
page_out = site.pages[PAGE_TITLE_OUT]

print(f"Fetch current page content: {PAGE_TITLE_IN}")
original_content = page_in.text()



# *Botten Ada: Samtliga opinionsmätningar, värderade efter bland annat historisk träffsäkerhet, mätperiod och urvalsstorlek.<ref name="ada">{{Webbref|titel=FAQ – Så fungerar Botten Ada|url=https://www.bottenada.se/faq|verk=www.bottenada.se|hämtdatum=2023-08-28|språk=sv}}</ref>
# 

# In[8]:


expected_party_order = [
    ("Vänsterpartiet", "V"),
    ("Socialdemokraterna", "S"),
    ("Miljöpartiet", "MP"),
    ("Centerpartiet", "C"),
    ("Liberalerna", "L"),
    ("Moderaterna", "M"),
    ("Kristdemokraterna", "KD"),
    ("Sverigedemokraterna", "SD"),
]

def validate_table(table_content):
    rows = table_content.split("|-\n")
    header_row = rows[1]
    parties_row = rows[3]
    header_cols = header_row.split("\n")[:-1]

    # Validera kolumnrubriker
    expected_header_cols = [
        "Institut",
        "Publicerad/<br>Period",
        "Riksdagspartier",
        "Andra",
        "{{nowrap|V, S, MP, C}}",
        "{{nowrap|L, M, KD, SD}}",
        "Diff",
        "Källa",
    ]

    for ix, header in enumerate(header_cols):
        if header.strip() == "":
            continue
        if expected_header_cols[ix] not in header:
            raise ValueError(f"Unexpected header value: '{header}'. Expected '{expected_header_cols[ix]}'")


    # Validera partiordning
    party_cols = parties_row.split("\n")[:-1]

    for ix, party in enumerate(party_cols):
        if party.strip() == "":
            continue
        if expected_party_order[ix][0] not in party:
            raise ValueError(f"Unexpected party: '{party}'. Expected '{expected_party_order[ix][0]}'")



# Locate the section "Sammanvägningar"
section_start = original_content.find("== Sammanvägningar ==")
if section_start == -1:
    raise ValueError("Section not found")

# Find the start and end of the table within the section
table_start = original_content.find("{|", section_start)
table_end = original_content.find("|}", table_start)
if table_start == -1 or table_end == -1:
    raise ValueError("Table not found")

# Extract the table content
table_content = original_content[table_start:table_end]

# Make sure col order is as expected
validate_table(table_content)
print("Existing table is valid.")

# Read data
url = DATA_URL + "latest/seats--all.json"
print(f"Get {url}")

r = requests.get(url)
seats_json = r.json()


def format_percent(value):
    # Multiply the float by 100 and round it to one decimal place
    percent_value = round(value * 100, 1)
    # Format the rounded value as a string with one decimal place
    return f"{percent_value:.1f} %".replace(".", ",")

def bold_if(string, condition):
    return f"'''{string}'''" if condition else string


def update_table(table, seats_json):
    # Parse rows
    rows = table.split("|-\n")
    header_rows = rows[:5]
    data_rows = rows[5:]

    # Make sure that this is the newer than any existing Ada data
    publ_date = datetime.strptime(seats_json["metadata"]["created"].split("T")[0], "%Y-%m-%d")
    for row in data_rows:
        # Empty rows
        if "||||||||||||||||" in row:
            continue
        
        cells = row.split("\n|")
        if "Botten Ada" in cells[1]:
            date_str = cells[2].split("|")[-1].replace("}","").strip()
            date = datetime.strptime(date_str, "%d %B %Y")
            if date >= publ_date:
                raise ValueError("A later Ada run has already been published") 

    # Build row
    C_S_V_MP = seats_json['now']['C_S_V_MP']['votes']['p50']
    M_L_KD_SD = seats_json['now']['M_L_KD_SD']['votes']['p50']
    other = 1 - C_S_V_MP - M_L_KD_SD

    new_data_row = "|-\n| " + "\n| ".join([
        # Institut
        "Botten Ada", 

        # Publiceringsdatum
        'style="text-align: left;" | {{Nowrap|' + publ_date.strftime('%-d %B %Y') + '}}', 
    ] \
        # Partierna 
    + [ f'<!-- {party} -->{format_percent(seats_json["now"][party]["votes"]["p50"])}' 
        for _, party in expected_party_order  ]\
    + [
        f"<!-- Övriga --> { format_percent(other)  }",
        # Koalitionerna
        f"<!-- S+Mp+V+C --> { bold_if(format_percent(C_S_V_MP), C_S_V_MP > M_L_KD_SD) }",
        f"<!-- M+KD+L+SD -->{ bold_if(format_percent(M_L_KD_SD), M_L_KD_SD > C_S_V_MP) }",
        f"bgcolor=\"{{{{ { 'Rödgröna/meta/färg' if C_S_V_MP > M_L_KD_SD else 'Borgerliga/meta/färg' } }}}}\"  | '''{ format_percent(abs(C_S_V_MP - M_L_KD_SD))}'''",

        # Referenslänk
        "|<ref name='ada'/>",
    ])
    empty_row = "||||||||||||||||"

    # TODO: Check that no other poll of polls is new and place accordingly
    data_rows.insert(0, new_data_row)
    # TODO: Add empty row if new month
    #data_rows.insert(1, empty_row)
    updated_table_str = "\n|-\n".join(header_rows + data_rows)

    return updated_table_str



def replace_substring(original_str, start_pos, end_pos, new_str):
    # Extract the parts before and after the positions
    before = original_str[:start_pos]
    after = original_str[end_pos:]
    
    # Concatenate the parts with the new string
    replaced_str = before + new_str + after
    
    return replaced_str

print("Update table")
updated_table_str = update_table(table_content, seats_json)
new_content = replace_substring(original_content, table_start, table_end, updated_table_str)

print(f"Update {PAGE_TITLE_OUT}")
page_out.edit(new_content, COMMIT_MSG)


# In[120]:





# In[ ]:





# In[91]:





# In[ ]:





# In[ ]:





# In[ ]:




