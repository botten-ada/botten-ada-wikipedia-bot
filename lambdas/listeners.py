from subprocess import call

def site_update(event, context):
    """Listens for updates on Ada site.
    """
    updated_file = event["Records"][0]["s3"]["object"]["key"]

    if updated_file.endswith("seats--all.json"):
        print("Call for a wikipedia update")
        cmd = ["python3",
            "update-wikipedia.py",
        ]
        print(u"CMD " + " ".join(cmd))
        exit_code = call(cmd)
        if exit_code != 0:
            raise Exception("Something went wrong")

    else:
        print(f"Not taking any action on {updated_file} updates.")
