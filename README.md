# Botten Ada Wikipedia Bot

A simple script that updates Wikipedia whenever the Botten Ada site is update.

## Develop

**Important**: Use the `update-wikipedia.ipynb` notebook to develop and run the script locally. `update-wikipedia.py` is just a `py` export of this notebook and not intended to be edited.

## Deploy

Deploys to AWS Lambda with Serverless. Run `make deploy` to export notebook a `.py` file and upload.