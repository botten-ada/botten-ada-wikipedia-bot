
import os
from dotenv import load_dotenv

if os.path.exists(".env"):
    load_dotenv(".env")

# Wikipedia credentials
USERNAME = os.environ["WIKI_USERNAME"]
PASSWORD = os.environ["WIKI_PASSWORD"]

# Where is Ada data stored?
ADA_S3_BUCKET = "ada-site-data"
ADA_S3_REGION = "eu-north-1"